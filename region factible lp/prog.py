import itertools as co
import numpy as np
import matplotlib.pyplot as plt
import sympy as sy


linea=["x1 : pantalon","x2:camisa","max z = 50x1 + 40x2","2x1+3x2 <= 1500","2x1+x2 <= 1000","x1=>0","x2=>0"]

x1 = sy.Symbol("x1")
x2 = sy.Symbol("x2")

fm=[] #matriz #no se usa para nada
fo="" #no se usa para nada
fr=[] # restricciones creo strin
frm=[] ##no se #otro lado de las restricciones matriz#no se usa para nada
frm.append(0)
sol=[]
fob=None
frs=[]## simbolo restriccion no se usa
frs.append("hola")
f=[] #funciones para comprobar etcx
fp=[] #para buscar los puntos
pf=[]
pfeva=[]
ejes=[[0,0]]##ejes para evaluar
ejex=[]#
ejey=[]# estas estan demas podria hacerlo todo en el eje x pero me da flojera buscarlas y borrarlas
interp=[]#eje de las inercepciones

def dibujar():

     color=["b","g","r","c", "m", "y", "k", "w"]
     #print("ff")
     ejelim=max(max(ejes))+max(max(ejes))*0.20
     poli=pf
     if(poli!=[]):
          poli.sort()
          pol = plt.Polygon(poli, closed=True, alpha=0.5)
     ax = plt.gca()
     ax.cla()
     #ax.set_xlim((0, ejelim))
     #ax.set_ylim((0, ejelim))
     ax.set_aspect('equal')
     fig = plt.gcf()
     if(pf!=[]):
          fig.gca().add_artist(pol)
     y,x=np.ogrid[0:ejelim:100j,0:ejelim:100j]
     ind=0
     for i in fp:
          #print(ind)
          #print(i)
          try:
               cs=ax.contour(x.ravel(),y.ravel(),i(x,y),0,colors=color[ind])##try aca
               cs.collections[0].set_label(fr[ind])
               ind+=1
          except:
               continue

     if(pf!=[]):
          
          for i in pf:
               lol="("+str(round(i[0],2))+", "+str(round(i[1],2))+")"
               ax.plot(round(i[0]),round(i[1]),'ko',lw=2.0)
               ax.annotate(lol, xy=(i[0],i[1]), xytext=(-5, 5), ha='right',textcoords='offset points')

          
          if(pfeva.count(sol[0])==1):
               lol="F(p):"+str(round(sol[0],3))+" P=:"+"x="+str(round(sol[1][0],2))+", y="+str(round(sol[1][1],2))
               ax.plot(round(sol[1][0],2), round(sol[1][1],2), 'go',markersize=10,label=lol )
               ax.set_title(linea[2]+':optimo encontrado')
          else:
               ax.set_title(linea[2]+':multiples soluciones')
     else:
          ax.set_title(linea[2]+':Problema sin solucion')
          
     plt.legend()
     ax.grid('on')
     plt.show()
     #acaba aca

def buscarp():

     #fp busca los puntos f los evalua
     #print("holadf")
     for i in fp:
          aux=sy.solve(i(x1,0),x1)
          if(aux==[]):
               aux=0
          else:
               aux=aux[0]
          ejex.append([aux,0])
          aux=sy.solve(i(0,x2),x2)
          if(aux==[]):
               aux=0
          else:
               aux=aux[0]
          #print(aux)
          ejey.append([0,aux])
     buscarinter()
     ##acaba aca

def buscarinter():
     
     auxfp=fp
     #print("intercep")
     iterable=list(range(len(fp)))
     j=co.combinations(iterable, 2)#combinacion de restricciones de kla lista
     for i in j:
          aux=[]
          g=sy.solve(auxfp[i[0]](x1,x2)-auxfp[i[1]](x1,x2),x1)
          if(g!=[]):
               g=g[0]
               try:
                    punto=float(g)
               except:
                    punto=sy.solve(auxfp[i[0]](g,x2),x2)
                    punto=punto[0]
                    aux.append(punto)
                    g=sy.solve(auxfp[i[0]](x1,punto),x1)
                    punto=g[0]
                    aux.append(punto)
                    interp.append(aux)

               else:
                    aux.append(punto)
                    g=sy.solve(auxfp[i[0]](punto,x2),x2)
                    punto=g[0]
                    aux.append(punto)
                    interp.append(aux)
          else:
               g=sy.solve(auxfp[i[0]](x1,x2)-auxfp[i[1]](x1,x2),x2)
               if(g!=[]):
                    g=g[0]
                    try:
                         punto=float(g)

                    except:

                         punto=sy.solve(auxfp[i[0]](x1,g),x1)
                         punto=punto[0]
                         aux.append(punto)
                         g=sy.solve(auxfp[i[0]](punto,x2),x2)
                         punto=g[0]
                         aux.append(punto)
                         interp.append(aux)
                    
                    else:
                         g=sy.solve(auxfp[i[0]](x1,punto),x1)
                         aux.append(g[0])
                         aux.append(punto)
                         interp.append(aux)
                    
               else:
                         
                    print("no choca")
                    continue
          #print(i)

     #acaba aca
def buscarpf(objetivo):
     
     ejes.extend([i for i in ejex if i not in ejes])#eliminar duplicados ejex
     ejes.extend([i for i in ejey if i not in ejes])# crea una lista con todos los puntos en el vertice
     ejes.extend([i for i in interp if i not in ejes])
     #print("holapf")
     sol.append(0)
     auxsol=sol[0]
     auxp=0
     aux=0
     for i in ejes:
          #print (i)
          val=True
          for j in f:
              #print("l")
              if((j(i[0],i[1]))):
                   continue
                   #print(j(i[0],i[1]))
              else:
                    val=False
                    break
          if(val):
               aux=fob(i[0],i[1])

               pf.append(i)
               pfeva.append(aux)

               if(objetivo):
                    
                    if(aux>auxsol):
                         auxsol=aux
                         auxp=i
               else:
                    if(aux<auxsol):
                         auxsol=aux
                         auxp=i
     sol[0]=auxsol
     sol[1]=auxp
     #acaba aca

def armafr(fun,sym):

     aux=fun.lower()
     faux=[]
     pos=0
     pos1=0

     if(aux.find("x1")<0 and aux.find("x2")<0):

          print("Error en la funcion restriccion")
          input("Press enter to ")
          exit(0)

     elif(aux.find("x1")<0):

          faux.append(0)

          if(aux.startswith("-")):
               
               aux="0*x1"+aux

          else:

               aux="0*x1+"+aux

          pos1=aux.find("x2")
          pos=aux.find("x1")
          

          if(aux[pos1-1].isdigit()):

               faux.append(float(aux[pos+2:pos1]))
               aux=aux[:pos1]+"*"+aux[pos1:]

          else:
     
               if(aux[pos1-1]=="-"):
                    faux.append(-1)
               else:
                    faux.append(1)

     elif(aux.find("x2")<0):

          pos=aux.find("x1")
          if(aux[pos-1].isdigit()and pos!=0):

               faux.append(float(aux[:pos]))
               aux=aux[:pos]+"*"+aux[pos:]

          else:
     
               if(aux[pos-1]=="-"):
                    faux.append(-1)
               else:
                    faux.append(1)
                    
          faux.append(0)
          aux=aux[:pos+2]+"+0*x2"+aux[pos+2:]

     else:

          pos=aux.find("x1")
          if(aux[pos-1].isdigit()and pos!=0):

               faux.append(float(aux[:pos]))
               aux=aux[:pos]+"*"+aux[pos:]

          else:
     
               if(aux[pos-1]=="-"):
                    faux.append(-1)
               else:
                    faux.append(1)
          
          pos1=aux.find("x2")
          pos=aux.find("x1")
          

          if(aux[pos1-1].isdigit()):

               faux.append(float(aux[pos+2:pos1]))
               aux=aux[:pos1]+"*"+aux[pos1:]

          else:
     
               if(aux[pos1-1]=="-"):
                    faux.append(-1)
               else:
                    faux.append(1)
                    
     frs.append(sym)
     fr.append(aux)
     fm.append(faux)

     aux1=aux.split(sym)
     frm.append(float(aux1[1]))
     aux=aux1[0]+"+"+str(float(aux1[1])*-1)

     fp.append(lambda x1,x2:eval(aux))#funcione para buscar los puntos

     if(sym=="=>"):
          aux1=aux1[0]+">="+aux1[1]
     elif(sym=="="):

          aux1=aux1[0]+"=="+aux1[1]

     else:
          aux1=aux1[0]+sym+aux1[1]
     
     
     return lambda x1,x2: eval(aux1)

def armaf(mul,fun):

     global fo
     aux=fun.lower()
     faux=[]
     pos=aux.find("x1")
     if(aux[pos-1].isdigit()and pos!=0):

          faux.append(float(aux[:pos])*mul)
          aux=aux[:pos]+"*"+aux[pos:]

     else:
     
          if(aux[pos-1]=="-"):
               faux.append(-1)
          else:
               faux.append(1)
          
     pos1=aux.find("x2")
     pos=aux.find("x1")
    

     if(aux[pos1-1].isdigit()):

          faux.append(float(aux[pos+2:pos1]*mul))
          aux=aux[:pos1]+"*"+aux[pos1:]

     else:
     
          if(aux[pos1-1]=="-"):
               faux.append(-1)
          else:
               faux.append(1)

     
     fm.append(faux)
     
     aux=aux.strip()
     fo=(aux)
     #print(aux)
     return lambda x1,x2: eval(aux)
     
def armar():

     aux=""
     global fob
     for i in linea:

          aux=i
          aux=aux.strip()
          aux=aux.replace(" ", "")

          if("z" in aux.lower()):

               aux=aux.split("=")
               if(len(aux)!=2):

                    print("Error en la funcion objetivo")
                    input("Press enter to continue")
                    exit(0)

               elif(aux[0].lower().startswith("max")):

                    obj=True
                    sol.append(0)
                   # print("hola")
                    fob=(armaf(1,aux[1]))
                    

     
               elif(aux[0].lower().startswith("min")):
                    
                    obj=False
                    sol.append(9999999999)
                    #print("hola12")
                    fob=(armaf(1,aux[1]))
     
               else:

                    print("Error en la funcion objetivo")
                    input("Press enter to continue")
                    exit(0)

          elif(":" in aux):

               #para las variables esas
               #algo aca
               print(aux)

          elif((("<"in aux)or(">"in aux)or("=" in aux)) and ("z" not in aux) and ("max" not in aux ) and ("min" not in aux)):
               
               #aca para las restricciones
               print(aux)
               if("<=" in aux):

                    f.append(armafr(aux,"<="))

               elif("=>" in aux):

                   f.append(armafr(aux,"=>"))

               elif(">" in aux):

                   f.append(armafr(aux,">"))

               elif("<" in aux):

                    f.append(armafr(aux,"<"))

               elif("=" in aux):

                    f.append(armafr(aux,"="))

               else:

                    print("Error en la restricciones")
                    input("Press enter to continue")
                    exit(0)

          else:

                    print("Error en la funcion objetivos")
                    input("Press enter to continue")
                    exit(0)

     buscarp()
     buscarpf(obj)
     print("Puntos factibles:")
     ind=0
     for i in pf:
          print("P=(",round(i[0],3),", ",round(i[1],3),"),F(p)=",pfeva[ind])
          ind+=1
     print("Sol:\nP=(",round(sol[1][0],3),", ",round(sol[1][1],3),"),F(p)=",sol[0])
                    
