 #<pyuic5 -x main.ui -o main.py tranformar
#>>> len(level.split('\n'))[len(line) for line in level.split('\n')]

import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from main import Ui_MainWindow
import prog2
                
class MainWindowProgram(QtWidgets.QMainWindow, Ui_MainWindow):

        textboxs=[]#»dinamic textboxs
        comboboxs=[]#same
        labels=[]
        lps=prog2.lp()

        def __init__(self, parent=None):

                QtWidgets.QMainWindow.__init__(self)
                self.setupUi(self)
                self.lineEditvar.setValidator(QtGui.QIntValidator())#int
                self.lineEditrest.setValidator(QtGui.QIntValidator())
                self.pushButton.clicked.connect(self.pushButton_clicked)
                self.cancelButton.clicked.connect(self.borrarWidget)
                self.pushButton_2.clicked.connect(self.resolver)
                self.statu(False)
                
        def pushButton_clicked(self):

                try:
                        variables=int(self.lineEditvar.text())
                        restricciones=int(self.lineEditrest.text())
                        if variables==0 or restricciones==0:
                                raise
                except Exception:
                        QtWidgets.QMessageBox.warning(self,"Error","Inserte numero valido")
                else:
                        self.borrarWidget()
                        self.accion.setEnabled(True)
                        self.label_6.setEnabled(True)
                        self.label_7.setEnabled(True)
                        self.pushButton_2.setVisible(True)
                        self.creaCamp(variables, restricciones)
                     
        def keyPressEvent(self, event):#cerrar con esc

                if event.key() == QtCore.Qt.Key_Escape:
                       self.close()

        def creaCamp(self, columnas, filas):

                py=15
                
                for i in range(0,filas+1):

                        textbox=[]
                        px=120
                        py+=25
                        for j in range(0,columnas):
                                if i==0:
                
                                        textbox.append(self.creatb(px,19))
                                        if (j+1)<columnas:
                                                text="X"+str(j+1)+" + "
                                        else:
                                                text="X"+str(j+1)
                                        self.labels.append(self.crealb(px+35,19,text))
                                        px+=60
                                else:
                                        textbox.append(self.creatb(px,py))
                                        if (j+1)<columnas:
                                                text="X"+str(j+1)+" +"
                                        else:
                                                text="X"+str(j+1)
                                                self.comboboxs.append(self.creacb(px+60,py))
                                                textbox.append(self.creatb(px+110,py))
                                        self.labels.append(self.crealb(px+35,py,text))
                                        px+=60                          

                        self.textboxs.append(textbox)
                        self.pushButton_2.move(190,py+30)
                                
        def creatb(self,px,py):

                tb=QtWidgets.QLineEdit(self.scrollAreaWidgetContents)
                tb.setValidator(QtGui.QIntValidator())#int
                tb.setGeometry(QtCore.QRect(px, py, 30, 20))
                tb.show()
                return tb

        def crealb(self,px,py,ind):

                lb=QtWidgets.QLabel(ind,self.scrollAreaWidgetContents)
                lb.setGeometry(QtCore.QRect(px, py, 30, 20))
                lb.show()
                return lb

        def creacb(self,px,py):

                cb=QtWidgets.QComboBox(self.scrollAreaWidgetContents)
                cb.addItems(["<=","=","=>"])
                cb.setGeometry(QtCore.QRect(px, py, 40, 20))
                cb.show()
                return cb

        def borrarWidget(self):
                
                #map(lambda x:x.fcn(),all))
                [i.deleteLater() for i in self.labels]
                [[j.deleteLater() for j in i] for i in self.textboxs]
                [i.deleteLater() for i in self.comboboxs]
                self.labels=[]
                self.textboxs=[]
                self.comboboxs=[]
                self.statu(False)

        def statu(self,stu):#visiblidad de labels y demas
                
                self.accion.setEnabled(stu)
                self.label_6.setEnabled(stu)
                self.label_7.setEnabled(stu)
                self.pushButton_2.setVisible(stu)
                self.Estandar.setVisible(stu)
                self.fase1.setVisible(stu)
                self.fase2.setVisible(stu)
                self.result1.setVisible(stu)
                self.result2.setVisible(stu)

        def resolver(self):
                m,arriba,basicas=self.armarM()
                self.rlabels(self.accion.currentText()+" Z= "+self.lps.estandar(m,arriba),self.Estandar)

                if self.accion.currentIndex()==0:#min o max
                        op=0
                else:
                        op=1

                if self.metod.currentIndex()==0:#m 2phase
                        result=self.lps.fase1(m,basicas,arriba,op)
                        self.result1.setText("Fase 1.")
                        self.result2.setText("Fase 2.")
                else:
                        result=self.lps.tecM(m,basicas,arriba,op)
                        self.result1.setText("M=1000")
                        self.result2.setText("Solucion.")

                result=result.split("labelf2")
                self.rlabels(result[0],self.fase1)
                self.rlabels(result[1],self.fase2)
                self.statu(True)
                

        def armarM(self):

                basicas=["Z "]
                #currentindex

                try:
                        m=[[float(j.text()) for j in i] for i in self.textboxs]
                except Exception:
                        QtWidgets.QMessageBox.warning(self,"Error","Inserte numero")
                else:
                        s=[0]
                        r=[0]
                        arriba=["X"+str(i+1) for i in range(len(m[0]))]
                        m[0].append(0)

                        for cb in self.comboboxs:
                                if cb.currentIndex()==0:
                                        s.append(1)
                                        r.append(0)
                                        basicas.append("S"+str(sum([abs(i) for i in s])))

                                elif cb.currentIndex()==1:
                                        s.append(0)
                                        r.append(1)
                                        basicas.append("R"+str(sum(r)))
                                else:
                                        s.append(-1)
                                        r.append(1)
                                        basicas.append("R"+str(sum(r)))

                        for i,l in enumerate(s):
                                for j in range(len(m)):
                                        if l!=0:
                                                if i!=j:
                                                        m[j].insert(-1,0)
                                                else:
                                                        m[j].insert(-1,l)                         

                        for i,l in enumerate(r):
                                for j in range(len(m)):
                                        if l!=0:
                                                if i!=j:
                                                        m[j].insert(-1,0)
                                                else:
                                                        m[j].insert(-1,l)


                        arriba.extend(["S"+str(i+1) for i in range(sum([abs(i) for i in s]))])
                        arriba.extend(["R"+str(i+1) for i in range(sum(r))])
                        arriba.append("Sol")
                        return m,arriba,basicas

        def rlabels(self,text,label):#resize labels
                at=[len(text) for text in text.split('\n')]
                label.setMinimumSize(QtCore.QSize(max(at)*7, len(at)*13))
                label.setText(text)
                
        
if __name__ == '__main__':
        app = QtWidgets.QApplication(sys.argv)
        form = MainWindowProgram()
        form.show()
        sys.exit(app.exec_())
