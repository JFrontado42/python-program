# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created by: PyQt5 UI code generator 5.4.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(874, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(10, 0, 171, 271))
        self.groupBox.setObjectName("groupBox")
        self.metod = QtWidgets.QComboBox(self.groupBox)
        self.metod.setGeometry(QtCore.QRect(60, 20, 61, 22))
        self.metod.setToolTip("")
        self.metod.setObjectName("metod")
        self.metod.addItem("")
        self.metod.addItem("")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setGeometry(QtCore.QRect(20, 60, 47, 13))
        self.label.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.groupBox)
        self.label_2.setEnabled(True)
        self.label_2.setGeometry(QtCore.QRect(20, 90, 71, 16))
        self.label_2.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.label_2.setObjectName("label_2")
        self.lineEditvar = QtWidgets.QLineEdit(self.groupBox)
        self.lineEditvar.setGeometry(QtCore.QRect(90, 60, 71, 20))
        self.lineEditvar.setMaxLength(1)
        self.lineEditvar.setObjectName("lineEditvar")
        self.lineEditrest = QtWidgets.QLineEdit(self.groupBox)
        self.lineEditrest.setGeometry(QtCore.QRect(90, 90, 71, 20))
        self.lineEditrest.setMaxLength(1)
        self.lineEditrest.setObjectName("lineEditrest")
        self.pushButton = QtWidgets.QPushButton(self.groupBox)
        self.pushButton.setGeometry(QtCore.QRect(90, 130, 75, 21))
        self.pushButton.setObjectName("pushButton")
        self.cancelButton = QtWidgets.QPushButton(self.groupBox)
        self.cancelButton.setGeometry(QtCore.QRect(10, 130, 75, 21))
        self.cancelButton.setObjectName("cancelButton")
        self.scrollArea = QtWidgets.QScrollArea(self.centralwidget)
        self.scrollArea.setGeometry(QtCore.QRect(190, 10, 461, 261))
        self.scrollArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.scrollArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 442, 242))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.accion = QtWidgets.QComboBox(self.scrollAreaWidgetContents)
        self.accion.setGeometry(QtCore.QRect(40, 20, 51, 22))
        self.accion.setObjectName("accion")
        self.accion.addItem("")
        self.accion.addItem("")
        self.pushButton_2 = QtWidgets.QPushButton(self.scrollAreaWidgetContents)
        self.pushButton_2.setGeometry(QtCore.QRect(190, 70, 75, 23))
        self.pushButton_2.setObjectName("pushButton_2")
        self.label_6 = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_6.setGeometry(QtCore.QRect(100, 19, 47, 20))
        self.label_6.setObjectName("label_6")
        self.label_7 = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_7.setGeometry(QtCore.QRect(40, 50, 47, 13))
        self.label_7.setObjectName("label_7")
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.scrollArea_2 = QtWidgets.QScrollArea(self.centralwidget)
        self.scrollArea_2.setGeometry(QtCore.QRect(10, 280, 861, 301))
        self.scrollArea_2.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.scrollArea_2.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.scrollArea_2.setWidgetResizable(True)
        self.scrollArea_2.setObjectName("scrollArea_2")
        self.scrollAreaWidgetContents_2 = QtWidgets.QWidget()
        self.scrollAreaWidgetContents_2.setGeometry(QtCore.QRect(0, 0, 842, 282))
        self.scrollAreaWidgetContents_2.setObjectName("scrollAreaWidgetContents_2")
        self.fase1 = QtWidgets.QLabel(self.scrollAreaWidgetContents_2)
        self.fase1.setGeometry(QtCore.QRect(20, 50, 47, 13))
        self.fase1.setObjectName("fase1")
        self.fase2 = QtWidgets.QLabel(self.scrollAreaWidgetContents_2)
        self.fase2.setGeometry(QtCore.QRect(560, 50, 47, 13))
        self.fase2.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.fase2.setObjectName("fase2")
        self.result1 = QtWidgets.QLabel(self.scrollAreaWidgetContents_2)
        self.result1.setGeometry(QtCore.QRect(160, 20, 47, 13))
        self.result1.setObjectName("result1")
        self.result2 = QtWidgets.QLabel(self.scrollAreaWidgetContents_2)
        self.result2.setGeometry(QtCore.QRect(600, 20, 47, 13))
        self.result2.setObjectName("result2")
        self.label_8 = QtWidgets.QLabel(self.scrollAreaWidgetContents_2)
        self.label_8.setGeometry(QtCore.QRect(370, 10, 47, 13))
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.label_8.setFont(font)
        self.label_8.setWordWrap(False)
        self.label_8.setObjectName("label_8")
        self.scrollArea_2.setWidget(self.scrollAreaWidgetContents_2)
        self.scrollArea_3 = QtWidgets.QScrollArea(self.centralwidget)
        self.scrollArea_3.setGeometry(QtCore.QRect(660, 9, 211, 261))
        self.scrollArea_3.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.scrollArea_3.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.scrollArea_3.setWidgetResizable(True)
        self.scrollArea_3.setObjectName("scrollArea_3")
        self.scrollAreaWidgetContents_3 = QtWidgets.QWidget()
        self.scrollAreaWidgetContents_3.setGeometry(QtCore.QRect(0, 0, 192, 242))
        self.scrollAreaWidgetContents_3.setObjectName("scrollAreaWidgetContents_3")
        self.Estandar = QtWidgets.QLabel(self.scrollAreaWidgetContents_3)
        self.Estandar.setGeometry(QtCore.QRect(10, 20, 47, 13))
        self.Estandar.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.Estandar.setObjectName("Estandar")
        self.label_3 = QtWidgets.QLabel(self.scrollAreaWidgetContents_3)
        self.label_3.setGeometry(QtCore.QRect(10, 0, 81, 16))
        self.label_3.setObjectName("label_3")
        self.scrollArea_3.setWidget(self.scrollAreaWidgetContents_3)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 874, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.groupBox.setTitle(_translate("MainWindow", "Metodo"))
        self.metod.setCurrentText(_translate("MainWindow", "2 fases"))
        self.metod.setItemText(0, _translate("MainWindow", "2 fases"))
        self.metod.setItemText(1, _translate("MainWindow", "M"))
        self.label.setText(_translate("MainWindow", "Variables:"))
        self.label_2.setText(_translate("MainWindow", "Restricciones:"))
        self.pushButton.setText(_translate("MainWindow", "Ok"))
        self.cancelButton.setText(_translate("MainWindow", "Cancelar"))
        self.accion.setItemText(0, _translate("MainWindow", "Min"))
        self.accion.setItemText(1, _translate("MainWindow", "Max"))
        self.pushButton_2.setText(_translate("MainWindow", "Ok"))
        self.label_6.setText(_translate("MainWindow", "Z ="))
        self.label_7.setText(_translate("MainWindow", "S.A:"))
        self.fase1.setText(_translate("MainWindow", "TextLabel"))
        self.fase2.setText(_translate("MainWindow", "TextLabel"))
        self.result1.setText(_translate("MainWindow", "TextLabel"))
        self.result2.setText(_translate("MainWindow", "TextLabel"))
        self.label_8.setText(_translate("MainWindow", "Resultado"))
        self.Estandar.setText(_translate("MainWindow", "TextLabel"))
        self.label_3.setText(_translate("MainWindow", "Forma Estandar"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

