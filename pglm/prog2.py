class lp:
        def estandar(self,matriz,variables):
                text=""
                sa=True
                for i in matriz: 
                        for j,k in enumerate(i):
                                if variables[j]==variables[-1]:
                                        text+=" = "+str(k)+"\n"
                                else:
                                        if k<0:
                                                text+="   "+str(k)+variables[j]
                                        else:
                                                text+=" + "+str(k)+variables[j]
                        if sa:
                                sa=False
                                text+="\nS.a:\n\n"
                return text

        def printTable(self,matriz,arri,basi,text):
                text+=("-----------------------------------------------\n")
                text+="\t*"
                text += ("*\t*").join(arri)       
                text+= "*\n"
                for i,j in enumerate(matriz):
                        text+=basi[i] + ":\t"
                        for k in j:
                                text+="|"+str(round(k,2))+"|\t"
                                
                        text+="\n"

                text+=("-----------------------------------------------\n\n")
                return text

        def simple(self,matriz,basi,arriba,text):
                ac=False
                opt=False
                ite=0

                while (not ac) and (not opt):
                        pivocol=max(matriz[0][:-1])
                        if pivocol>0:
                                posentra=matriz[0].index(pivocol)
                                entra=arriba[posentra]
                                opt=False
                        else:
                                opt=True

                        if not opt:
                                aux=[j[-1]/j[posentra] if j[posentra]>0 and j[-1]!=0 else None for j in matriz[1:]]

                                if any(aux):
                                        posale=aux.index(min(i for i in aux if i is not None))+1
                                        sale=basi[posale]
                                        ac=False
                                else:
                                     ac=True

                        if (not ac) and (not opt):
                                ite+=1
                                text+="Entra: "+entra+", Sale: "+sale+"\n"
                                text+="Iteracion: "+str(ite)+"\n"
                                text+= "Elemento pivote: "+str(matriz[posale][posentra])+"\n"
                                matriz=self.pivotea(matriz,posentra,posale)
                                basi.insert((basi.index(sale)+1),entra)
                                basi.remove(sale)
                                text=self.printTable(matriz,arriba,basi,text)

                return matriz,basi,text

        def pivotea(self,matriz,entra,sale):

                pivote=matriz[sale][entra]
                for i,j in enumerate(matriz[sale]):
                        matriz[sale][i]=j/pivote

                col=[row[entra] for row in matriz]#si no, no se mantiene el valor para multiplicar

                for i,l in enumerate(col):
                        if i!=sale:
                                for j,x in enumerate(matriz[i]):
                                        matriz[i][j]=round((x-(l*matriz[sale][j])),5)

                return matriz

        def fase1(self,matriz, basicas, arriba,op):

                reglon0=[]
                arrib=[]
                text=self.printTable(matriz,arriba,basicas,"")
                text+="Fase 1\n"
                rr=[0 for i in arriba]
                matriax=[row for row in matriz]
                
                for i,j in enumerate(arriba):
                        if "R" not in j:
                                reglon0.append(matriz[0][i])
                                arrib.append(j)
                
                for i,j in enumerate(basicas):
                        if "R" in j:
                                for m,k in enumerate(matriz[i]):
                                        if "R" not in arriba[m]:
                                                rr[m]+=k
                                                matriax[0][m]=rr[m]

                text=self.printTable(matriax,arriba,basicas,"")                                
                matriax,basis,text=self.simple(matriax,basicas,arriba,text)

                if len(reglon0)==len(arriba):#no hay r
                        text+="labelf2Solucion: "
                        text+=str(matriax[0][-1])
                elif matriax[0][-1]<=0:
                        matrix=[[i[j] for j,k in enumerate(arriba) if "R" not in k]for i in matriax[1:]]
                        matrix.insert(0,reglon0)
                        text=self.fase2(matrix,basis,arrib,text,op)
                else:
                        text+="labelf2El problema no tiene solucion."

                return text

        def fase2(self,matriz,basicas,arriba,text,op):

                reglon0=[0 for i in arriba]
                text+="labelf2Matriz para fase 2.\n"

                for i,m in enumerate(basicas):
                        if "X" in m:
                                for j,k in enumerate(arriba):
                                        if k!=m:
                                                reglon0[j]+=matriz[i][j]*matriz[0][arriba.index(m)]

                if op!=0:
                        reglon0=[row*-1 for row in reglon0]

                matriz[0]=reglon0
                text=self.printTable(matriz,arriba,basicas,text)
                matriz,basicas,text=self.simple(matriz,basicas,arriba,text)
                text+="Solucion: "

                text+=str(matriz[0][-1])

                return text

        def tecM(self, matriz,basicas,arriba,op):

                text=self.printTable(matriz,arriba,basicas,"")
                text+="Operaciones:\n"
                rr=[0 for i in arriba]
                matriax=[row for row in matriz]
                M=1000
                
                for i,j in enumerate(basicas):
                        if "R" in j:
                                for n,k in enumerate(matriz[i]):
                                        if "R" not in arriba[n]:
                                                rr[n]+=k*M
                                                matriax[0][n]=rr[n]

                if op!=0:
                        matriax[0]=[row*-1 for row in matriax[0]]
                        
                text=self.printTable(matriax,arriba,basicas,"")
                matriax,basis,text=self.simple(matriax,basicas,arriba,text)
                text+="labelf2Solucion: "
                text+=str(matriax[0][-1])

                return text
        
