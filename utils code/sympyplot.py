from sympy import plot_implicit, symbols
x1, x2 = symbols('x1 x2')
 
plot_implicit(((2*x1+3*x2)<1500))
