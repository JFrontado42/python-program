import matplotlib.pyplot as plt
import numpy as np
p1, p2, p3, p4 = [[0, 0], [0, 500], [375, 250], [500, 0]]
pol = plt.Polygon([p1, p2, p3, p4], closed=True, alpha=0.5)
ax = plt.gca()
ax.cla()
ax.set_xlim((0, 1000))
ax.set_ylim((0, 1000))
ax.set_aspect('equal')
fig = plt.gcf()
fig.gca().add_artist(pol)
t = np.linspace(0, 1000, 100)
ax.plot(t, (1500-2*t) / 3., color='red', lw=2.0)
ax.plot(t, 1000-2*t, color='green', lw=2.0)
ax.plot(round(375), round(250), 'ko', lw=2.0)
ax.set_title('Problema Grandes Almacenes')
plt.legend([r'$2x+3y=1500$', r'$2x+y=1000$', r'$P=(375,\; 250)\; f(P)=28750$'])

ax.grid('on')
plt.show()
