from sympy.solvers import solve
from sympy import Symbol
import sympy as sy
import itertools

x = Symbol('x')
y =Symbol('y')
eu = 2*x + 5*y - 30
def f1(x,y):
    return 40*x+50*y-400
def f2(x,y):
    return x+y -9
g = solve(f2(x,y)-f1(x,y),x)
k=[1,3,2,4]
iterable=list(range(len(k)))
j=itertools.combinations(iterable, 2)
for i in j:
    print(i)
    print(k[i[0]])
